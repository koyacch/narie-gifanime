        $(function() {
            //切り替えまでの時間。
            var settimer = 2200;
            //遷移先のurl
            var seturl = 'http://yahoo.co.jp/';

            $('.start').css({
                'position': 'relative',
                'cursor': 'pointer'
            });
            $('.start img[src*=".png"]').css({
                'position': 'absolute',
                'top': '0',
                'left': '0',
                'z-index': '100'
            });
            var animegif = $('.start img[src*=".png"]').attr('src');
            animegif = animegif.replace('.png', '.gif')


            $('.start').on('click', function() {

                var setimg = $('img').attr('src', animegif);
                setimg.load(function() {

                    $('img[src*=".png"]', this).css('display', 'none');
                    $(this).append($('<img>').attr('src', animegif)).css('display', 'block');
                    setTimeout(function() {
                        $(location).attr('href', seturl);
                    }, settimer);


                });

            });
        });