# README #

静止画像をボタンにし、クリックした後動画に切り替え、一定時間後に別URLに移動するプラグイン。

### How do I get set up? ###

* 動画gifと、同じファイル名の静止画pngを用意する。
* 動画gifは繰り返しにしない。

* HTMLにGoogleよりJQueryを読み込む。
* HTMLにanimegifchange.jsを読み込む。
* HTMLに動画と静止画を設置するdivを配置。
* divにクラス .start を付与する。
* div内に静止画png画像を置く。(動画gifは置かない。)
* animegifchange.js 内で、移動までの時間と移動先URLを記述する。